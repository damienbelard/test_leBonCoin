package com.belard.test_leboncoin;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by dbelard on 27/09/2017.
 * Custom implementation of the Class Application. Necessary for Realm
 */

public class CustomApp extends Application {
    private static CustomApp mInstance;

    public static synchronized CustomApp getInstance() {

        return mInstance;
    }

    private static synchronized void setInstance(CustomApp instance) {
        mInstance = instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        RealmConfiguration configuration = new RealmConfiguration.Builder().build();
        Realm.setDefaultConfiguration(configuration);

        setInstance(this);
    }
}
