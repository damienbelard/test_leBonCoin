package com.belard.test_leboncoin;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.belard.test_leboncoin.data.network.pojos.Soundtrack;
import com.squareup.picasso.Picasso;

/**
 * Created by dbelard on 27/09/2017.
 * Activity displaying the details of a soundtrack
 */
public class DetailViewActivity extends BaseActivity {
    //Object Sountrack that will be displayed
    Soundtrack mSoundtrack;

    //UI variables
    ImageView mSoundtrackImage;
    TextView mSoundtrackId;
    TextView mSoundtrackAlbumId;
    TextView mSoundtrackTitle;
    TextView mSoundtrackUrl;
    TextView mSoundtrackThumbnailId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_view);

        setToolbar(R.id.detail_toolbar);
        setToolbarBackButton();

        mSoundtrack = getIntent().getParcelableExtra(HomeActivity.SOUNDTRACK);
        initInterface();
        initValues();
    }

    //MEthod to init UI variables
    private void initInterface() {
        mSoundtrackImage = (ImageView) findViewById(R.id.image_image);
        mSoundtrackId = (TextView) findViewById(R.id.image_id);
        mSoundtrackAlbumId = (TextView) findViewById(R.id.image_album_id);
        mSoundtrackTitle = (TextView) findViewById(R.id.image_title);
        mSoundtrackUrl = (TextView) findViewById(R.id.image_url);
        mSoundtrackThumbnailId = (TextView) findViewById(R.id.image_thumbnail_url);

    }

    //Method to set UI values
    private void initValues() {
        Picasso.with(this).load(mSoundtrack.getUrl()).into(mSoundtrackImage);
        mSoundtrackId.setText(String.format(getString(R.string.image_id_text), String.valueOf(mSoundtrack.getId())));
        mSoundtrackAlbumId.setText(String.format(getString(R.string.image_album_id_text), String.valueOf(mSoundtrack.getAlbumId())));
        mSoundtrackTitle.setText(String.format(getString(R.string.image_title_text), mSoundtrack.getTitle()));
        mSoundtrackUrl.setText(String.format(getString(R.string.image_url_text), mSoundtrack.getUrl()));
        mSoundtrackThumbnailId.setText(String.format(getString(R.string.image_thumbnail_url_text), mSoundtrack.getThumbnailUrl()));
    }
}
