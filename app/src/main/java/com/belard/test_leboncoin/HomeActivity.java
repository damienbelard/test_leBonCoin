package com.belard.test_leboncoin;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.belard.test_leboncoin.data.DataManager;
import com.belard.test_leboncoin.data.network.NetworkManager;
import com.belard.test_leboncoin.data.network.pojos.Soundtrack;
import com.belard.test_leboncoin.recyclerview.RecyclerTouchListener;
import com.belard.test_leboncoin.recyclerview.RecyclerViewClickListener;
import com.belard.test_leboncoin.recyclerview.SoundtrackAdapter;

import java.util.ArrayList;
import java.util.List;

import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.RealmResults;

/**
 * Activity displayed at launch, displaying the list of data retrieved from server
 */
public class HomeActivity extends BaseActivity {

    //Constant used for passing data in bundle
    public static final String RESULTS_LIST = "results_list";
    public static final String SOUNDTRACK = "soundtrack";

    //UI variables
    private RecyclerView recyclerView;
    private SwipeRefreshLayout mRefresh;

    //Other variables
    private List<Soundtrack> mSoundtrackslist;
    private SoundtrackAdapter mAdapter;
    private HomeActivity mActivity;

    //Listener plugged on a Realm query. Is called when the query is sent and when Realm notify that results of query has changed (Realm results are live, auto-updated)
    private OrderedRealmCollectionChangeListener<RealmResults<Soundtrack>> mChangeListener =  new OrderedRealmCollectionChangeListener<RealmResults<Soundtrack>>() {
        @Override
        public void onChange(RealmResults<Soundtrack> soundtracks, OrderedCollectionChangeSet changeSet) {
            //We get the new list and set it, then set adapter or notify it of the change. Finally, we hide the SwipeRefreshLayout
            mSoundtrackslist = soundtracks;
            if (changeSet == null) {
                setAdapter();
            } else {
                if (mAdapter != null) {
                    mAdapter.notifyDataSetChanged();
                }
            }
            mRefresh.setRefreshing(false);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        //W set the toolbar
        setToolbar(R.id.home_toolbar);
        mActivity = this;

        //We set the recyclerView and it's custom clickListener
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.hasFixedSize();
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, new RecyclerViewClickListener() {
            @Override
            public void onClick(View view, int position) {
                Intent intent = new Intent(getApplicationContext(), DetailViewActivity.class);
                intent.putExtra(SOUNDTRACK, mSoundtrackslist.get(position));
                //TEMPORARY FIX: List is too long to be saved when changing activity. We empty it before launching next activity
                mSoundtrackslist = new ArrayList<>();
                getApplicationContext().startActivity(intent);
            }
        }));

        //We set the SwipeRefreshLayout
        mRefresh = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        mRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                DataManager.getInstance(mActivity).getData(mChangeListener);
            }
        });

        //We check if it is the first launch or if we are back after rotation
        if (savedInstanceState != null) {
            mSoundtrackslist = savedInstanceState.getParcelableArrayList(RESULTS_LIST);
            setAdapter();

        } else {
            mRefresh.setRefreshing(true);
            mSoundtrackslist = new ArrayList<>();
            DataManager.getInstance(this).getData(mChangeListener);
        }
    }

    //TEMPORARY FIX: we check if list is empty and get the data back from Realm
    @Override
    protected void onStart() {
        super.onStart();
        if (mSoundtrackslist.isEmpty()) {
            DataManager.getInstance(this).restoreDataFromRealm(mChangeListener);
        }
    }

    //We set the adapter
    private void setAdapter() {
        mAdapter = new SoundtrackAdapter(mSoundtrackslist);
        recyclerView.setAdapter(mAdapter);
    }

    //We save data to handle case such as rotation
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        NetworkManager.getInstance(this).cancelInProgressCall();
        outState.putParcelableArrayList(RESULTS_LIST, new ArrayList<>(mSoundtrackslist));
    }
}
