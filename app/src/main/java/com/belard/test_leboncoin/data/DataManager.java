package com.belard.test_leboncoin.data;

import android.content.Context;

import com.belard.test_leboncoin.Utils;
import com.belard.test_leboncoin.data.network.NetworkManager;
import com.belard.test_leboncoin.data.network.pojos.Soundtrack;
import com.belard.test_leboncoin.data.realm.RealmManager;

import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.RealmResults;

/**
 * Created by dbelard on 27/09/2017.
 * Manager that handle all the data query
 */

public class DataManager {


    private static DataManager INSTANCE;
    private Context mContext;

    public static DataManager getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new DataManager();
        }
        INSTANCE.mContext = context.getApplicationContext();
        return INSTANCE;
    }

    //Method to get the data. Use the network status to choose to query either the server or the local Realm
    public void getData(OrderedRealmCollectionChangeListener<RealmResults<Soundtrack>> changeListener) {
        if (Utils.isNetworkOk(mContext)) {
            NetworkManager.getInstance(mContext).getSounds(changeListener);
        } else {
            RealmManager.getInstance().getData(changeListener);
        }
    }

    //Method to restore data from Realm
    public void restoreDataFromRealm(OrderedRealmCollectionChangeListener<RealmResults<Soundtrack>> changeListener) {
        if (!NetworkManager.getInstance(mContext).isCallInProgress()) {
            RealmManager.getInstance().getData(changeListener);
        }
    }
}
