package com.belard.test_leboncoin.data.network;

import android.content.Context;

import com.belard.test_leboncoin.data.network.pojos.Soundtrack;
import com.belard.test_leboncoin.data.realm.RealmManager;

import java.util.List;

import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Damien on 26/09/2017.
 * Manager singleton class to handle network requests
 */

public class NetworkManager {

    private static final String TAG = NetworkManager.class.getSimpleName();
    private static NetworkManager INSTANCE;
    private final Context mContext;
    private SoundService mWeatherService;
    private Call<List<Soundtrack>> mCall;
    private boolean mCallInProgress;

    private NetworkManager(Context mContext) {
        this.mWeatherService = RetrofitClient.getClient().create(SoundService.class);
        this.mContext = mContext.getApplicationContext();
    }

    public static NetworkManager getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new NetworkManager(context);
        }
        return INSTANCE;
    }

    //Method to request the results from openWeather and pass the results to the callback
    public void getSounds(final OrderedRealmCollectionChangeListener<RealmResults<Soundtrack>> changeListener) {
        mCallInProgress = true;
        mCall = mWeatherService.getSoundList();
        mCall.enqueue(new Callback<List<Soundtrack>>() {
            @Override
            public void onResponse(Call<List<Soundtrack>> call, Response<List<Soundtrack>> response) {
                mCallInProgress = false;
                RealmManager.getInstance().insertData(response.body(), changeListener);
            }

            @Override
            public void onFailure(Call<List<Soundtrack>> call, Throwable t) {
                mCallInProgress = false;
                //handle failure
            }
        });
    }

    //Method to cancel a call in progress if there is one
    public void cancelInProgressCall() {
        if (isCallInProgress()) {
            mCall.cancel();
        }
    }

    //Method to check if we have a call in progress
    public boolean isCallInProgress() {
        return mCallInProgress;
    }
}
