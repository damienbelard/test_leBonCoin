package com.belard.test_leboncoin.data.network;

import com.belard.test_leboncoin.BuildConfig;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Damien on 26/09/2017.
 * Class who initalise the Retrofit client
 */

class RetrofitClient {
    private static Retrofit retrofitClient = null;

    static Retrofit getClient() {
        if (retrofitClient == null) {
            retrofitClient = new Retrofit.Builder()
                    .baseUrl(BuildConfig.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofitClient;
    }
}
