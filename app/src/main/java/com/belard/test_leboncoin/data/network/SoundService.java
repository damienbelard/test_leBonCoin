package com.belard.test_leboncoin.data.network;


import com.belard.test_leboncoin.data.network.pojos.Soundtrack;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Damien on 26/09/2017.
 * Interface containing the Retrofit calls
 */

interface SoundService {

    @GET("urlwrap/?q=AXicHcxNDsIgEEDhORGlrZjISuNOl12YuCMDDTS0QwZawxVdeQrP4c_bfsmDMzzfAO4FwLG20orMm5hNiEhLYYoCaYa1v5zWe55ku5P7HrBy-IKxxK4cM84jmwWdGBl8KUk3zZRpSdGg8xStY1FqCkjW_W5N8lQow3a7DsrrIT5Qd0p2BwX_PrB9MO4&action=allow")
    Call<List<Soundtrack>> getSoundList();
}