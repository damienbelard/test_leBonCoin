package com.belard.test_leboncoin.data.realm;

import com.belard.test_leboncoin.data.network.pojos.Soundtrack;

import java.util.List;

import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by dbelard on 27/09/2017.
 * Manager to handle the Realm query
 */

public class RealmManager {

    private static RealmManager mRealmManagerInstance;

    public static RealmManager getInstance() {
        if (mRealmManagerInstance == null) {
            mRealmManagerInstance = new RealmManager();
        }
        return mRealmManagerInstance;
    }

    //Method to insert data into Realm then retrieve it to trigger the listener in HomeActivity
    public void insertData(final List<Soundtrack> results, final OrderedRealmCollectionChangeListener<RealmResults<Soundtrack>> changeListener) {
        Realm.getDefaultInstance().executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Realm.getDefaultInstance().delete(Soundtrack.class);
                List<Soundtrack> result = Realm.getDefaultInstance().copyToRealmOrUpdate(results);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                if (changeListener != null) {
                    getData(changeListener);
                }
            }
        });
    }

    //Method to get all the data from Realm
    public void getData(final OrderedRealmCollectionChangeListener<RealmResults<Soundtrack>> changeListener) {
        final RealmResults<Soundtrack> results = Realm.getDefaultInstance().where(Soundtrack.class).findAllAsync();
        results.addChangeListener(changeListener);
    }

    //Method to delete all the data from Realm
    public void deleteData() {
        Realm.getDefaultInstance().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Realm.getDefaultInstance().delete(Soundtrack.class);
            }
        });
    }
}
