package com.belard.test_leboncoin.recyclerview;

import android.view.View;

/**
 * Created by Damien on 27/09/2017.
 * Interface used for the recyclerView click listener
 */

public interface RecyclerViewClickListener {
    void onClick(View view, int position);
}
