package com.belard.test_leboncoin.recyclerview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.belard.test_leboncoin.R;
import com.belard.test_leboncoin.data.network.pojos.Soundtrack;

import java.util.List;

/**
 * Created by Damien on 27/09/2017.
 * This class is the adapter of the recyclerView displayed in the ListFragment. It has the charge of getting the data and populate it in the recyclerView
 */

public class SoundtrackAdapter extends RecyclerView.Adapter<SoundtrackViewHolder> {

    private List<Soundtrack> mSoundtrackList;
    private Context context;

    public SoundtrackAdapter(List<Soundtrack> list) {
        this.mSoundtrackList = list;
    }

    @Override
    public SoundtrackViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item, parent, false);
        return new SoundtrackViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SoundtrackViewHolder holder, int position) {
        holder.bind(mSoundtrackList.get(position), context);
    }

    @Override
    public int getItemCount() {
        return mSoundtrackList.size();
    }


}
