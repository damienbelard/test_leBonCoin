package com.belard.test_leboncoin.recyclerview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.belard.test_leboncoin.R;
import com.belard.test_leboncoin.data.network.pojos.Soundtrack;
import com.squareup.picasso.Picasso;

/**
 * Created by Damien on 27/09/2017.
 * Custom viewholder for the recyclerView
 */
class SoundtrackViewHolder extends RecyclerView.ViewHolder {

    private TextView mTvTitle;
    private ImageView mIvThumbnail;

    SoundtrackViewHolder(View itemView) {
        super(itemView);

        mTvTitle = (TextView) itemView.findViewById(R.id.item_title);
        mIvThumbnail = (ImageView) itemView.findViewById(R.id.item_image);
    }

    void bind(Soundtrack soundtrack, Context context) {
        Picasso.with(context).load(soundtrack.getThumbnailUrl()).into(mIvThumbnail);
        mTvTitle.setText(soundtrack.getTitle());
    }
}
